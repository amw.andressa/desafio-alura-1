import java.util.Scanner;

public class Desafio {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String nome = "Jacqueline Oliveira";
        String tipoConta = "Corrente";
        double saldoDaConta = 2500.00;

        System.out.println("***************************");
        System.out.println("Nome:           " + nome);
        System.out.println("Tipo Conta:     " + tipoConta);
        System.out.println("Saldo inicial:  R$ " + saldoDaConta);
        System.out.println("\n***************************");

        int operacao = 0;
        while (operacao != 4) {
            System.out.println("1 - Consultar saldo");
            System.out.println("2 - Receber valor");
            System.out.println("3 - Transferir valor");
            System.out.println("4 - Sair");
            System.out.println("\nDigite a opção desejada: ");
            operacao = sc.nextInt();

            if (operacao == 1) {
                System.out.println("--------------------------- ");
                System.out.println("Saldo disponível: R$ " + saldoDaConta);
                System.out.println("---------------------------\n");

            } else {
                if (operacao == 2) {
                    System.out.println("Digite o valor a receber: ");
                    //saldo atual = o saldo existente + o valor a receber
                    double valorAReceber = sc.nextDouble();
                    saldoDaConta += valorAReceber;
                    System.out.println("--------------------------- ");
                    System.out.println("Saldo atualizado: R$ " + saldoDaConta);
                    System.out.println("--------------------------- ");

                } else {
                    if (operacao == 3) {
                        System.out.println("Informe o valor a ser transferido: ");
                        double valorATransferir = sc.nextDouble();
                        if (valorATransferir > saldoDaConta) {
                            System.out.println("O valor excede o que tem disponível em conta. " +
                                    "Informe novo valor:");
                            valorATransferir = sc.nextDouble();
                            saldoDaConta -= valorATransferir;
                            System.out.println("--------------------------- ");
                            System.out.println("Valor transferido: R$ " + valorATransferir);
                            System.out.println("Saldo atualizado: R$" + saldoDaConta);
                            System.out.println("--------------------------- ");
//                            if(saldoDaConta == 0){
//                                System.out.println("Você não possui mais dinheiro em conta.");
//                            }
                        }
                    } else if(operacao != 4){
                        System.out.println("--------------------------- ");
                        System.out.println("Operação inválida!");
                        System.out.println("--------------------------- ");

                    }
                }
            }
        }
    }
}